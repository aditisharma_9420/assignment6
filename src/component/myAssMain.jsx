import React, { Component } from "react";
import { nativeTouchData } from "react-dom/test-utils";

class MainComponent extends Component {
    state = {
        persons: ["France", "England", "Brazil", "Germany", "Argentina"],
        TeamDetails: [],
        pointTable: [
            { teamName: "France", played: 0, won: 0, loss: 0, draw: 0, goalFor: 0, goalAgainst: 0, points: 0 },
            { teamName: "England", played: 0, won: 0, loss: 0, draw: 0, goalFor: 0, goalAgainst: 0, points: 0 },
            { teamName: "Brazil", played: 0, won: 0, loss: 0, draw: 0, goalFor: 0, goalAgainst: 0, points: 0 },
            { teamName: "Germany", played: 0, won: 0, loss: 0, draw: 0, goalFor: 0, goalAgainst: 0, points: 0 },
            { teamName: "Argentina", played: 0, won: 0, loss: 0, draw: 0, goalFor: 0, goalAgainst: 0, points: 0 },
        ],
        view: 0,
        team1: "",
        team2: "",
        team1Score: 0,
        team2Score: 0,
        points: 0,

    }
    //================================state finished======================================================================================
    //=====================================Functions=====================================================================================
    //====================================GAme Over=====================================================================================
    gameOver = () => {

        let s1 = { ...this.state };
        let obj = {}
        obj.team1 = s1.team1
        obj.team2 = s1.team2
        obj.team1Score = s1.team1Score
        obj.team2Score = s1.team2Score
        obj.result = s1.team1Score > s1.team2Score ? s1.team1 + " won" : s1.team2Score > s1.team1Score ? s1.team2 + " won" : s1.team1Score == s1.team2Score ? "Match draw" : ""
        s1.TeamDetails.push(obj)
        console.log("team 1 Push===>" + s1.team1)
        console.log("team 2 push===>" + s1.team2)
        console.log("team1 score ===>" + s1.team1Score)
        console.log("team 2 score===>" + s1.team2Score)
        console.log(s1.TeamDetails)
        console.log(obj)
        s1.view = 0;
        s1.team1Score = 0
        s1.team2Score = 0
        s1.team1 = ""
        s1.team2 = ""
        this.pointsTable()
        this.setState(s1);
    }
    //=====================================================Points Table===================================================
    pointsTable = () => {
        let s1 = { ...this.state };
        s1.persons.map((p1) => {
            console.log(p1)
            let goalAgainst = 0
            let goalFor = 0
            let points = 0
            let won = 0
            let drawn = 0
            let loss = 0
            let matchPlayed = 0
            s1.TeamDetails.map((t1) => {
                if (t1.team1 == p1) {
                    if ((t1.team1 == p1) && (p1 == s1.team1)) {
                       console.log("matchPlayed for ", p1, " ==>>", points, won, loss, drawn, matchPlayed)
                       matchPlayed=matchPlayed+1
                   }
                    if ((t1.team1 == p1) && t1.result == p1 + " won" && (p1 == s1.team1)) {
                        won += 1
                        points += 3
                        goalFor += t1.team1Score
                    } else if ((t1.team1 == p1) && t1.result == "Match draw" && (p1 == s1.team1)) {
                        drawn += 1
                        points += 1
                        goalFor += t1.team1Score
                    }
                    loss = matchPlayed - won-drawn

                    if (t1.team1 == p1 && (p1 == s1.team1)) {
                        let index = s1.pointTable.findIndex((e) => e.teamName == p1)
                        s1.pointTable[index].played += matchPlayed
                        s1.pointTable[index].won += won
                        s1.pointTable[index].loss += loss
                        s1.pointTable[index].goalFor += s1.team1Score
                        s1.pointTable[index].goalAgainst += s1.team2Score
                        s1.pointTable[index].points += points
                        s1.pointTable[index].draw += drawn
                        console.log("==>>>", s1.pointTable[index])
                    }
                } else if (t1.team2 == p1) {
                    if ((t1.team2 == p1) && (p1 == s1.team2)) {
                        console.log("matchPlayed for ", p1, " ==>>", points, won, loss, drawn, matchPlayed)
                        matchPlayed =matchPlayed+1
                    }
                    if ((t1.team2 == p1) && t1.result == p1 + " won" && (p1 == s1.team2)) {
                        won += 1
                        points += 3
                        goalFor += t1.team2Score
                    } else if ((t1.team2 == p1) && t1.result == "Match draw" && (p1 == s1.team2)) {
                        drawn += 1
                        points += 1
                        goalFor += t1.team2Score
                    }
                    loss = matchPlayed - won-drawn
                    goalAgainst = t1.team2Score
                    if (t1.team2 == p1 && (p1 == s1.team2)) {
                        let index = s1.pointTable.findIndex((e) => e.teamName == p1)
                        s1.pointTable[index].played += matchPlayed
                        s1.pointTable[index].won += won
                        s1.pointTable[index].loss += loss
                        s1.pointTable[index].goalFor += s1.team2Score
                        s1.pointTable[index].goalAgainst += s1.team1Score
                        s1.pointTable[index].points += points
                        s1.pointTable[index].draw += drawn
                        console.log("==>>>", s1.pointTable[index])
                    }
                }
            })
        })
        this.setState(s1);
    }

    sortPTBy = (sortBy) => {
        console.log(sortBy)
        let s1 = { ...this.state }

        let newPT = s1.pointTable.sort((a, b) => a[sortBy] > b[sortBy] ? -1 : 1)
        s1.pointTable = newPT
        this.setState(s1)
    }


    //================================================== for start Match=====================================================
    showForm = () => {
        let s1 = { ...this.state };
        s1.view = 1;
        this.setState(s1);
    }
    //================================================All Matches Details Table==================================================
    showList = () => {
        let s1 = { ...this.state };
        s1.view = 2;
        this.setState(s1);
    }
    //==================================================for Point table=========================================================
    showPT = () => {
        // this.pointsTable()
        let s1 = { ...this.state };
        s1.view = 5;
        this.setState(s1);
    }
    //================================================for Start MAtch Button==========================================================
    buzzerClick = () => {
        let s1 = { ...this.state };
        if (s1.team1 == "") {
            alert("choose team 1")
            s1.view = 1;
        }
        else if (s1.team2 == "") {
            alert("choose team 2")
            s1.view = 1;
        }
        else if (s1.team1 == s1.team2) {
            alert("choose diffrent team ")
            s1.view = 1;
        }

        else {
            s1.view = 4;
        }


        this.setState(s1);
    }

    //======================================================Team 1&2 value============================================================
    setTeam1 = (teamName) => {
        let s1 = { ...this.state }
        s1.team1 = teamName
        console.log(teamName)


        this.setState(s1)
    }
    setTeam2 = (teamName) => {
        let s1 = { ...this.state }
        s1.team2 = teamName
        console.log(teamName)
        this.setState(s1)
    }
    //============================================================Goal increment============================================================
    goalTeam1 = () => {
        let s1 = { ...this.state }
        s1.team1Score = s1.team1Score + 1;
        this.setState(s1)
    }
    goalTeam2 = () => {
        let s1 = { ...this.state }
        s1.team2Score = s1.team2Score + 1;
        this.setState(s1)
    }
    //=======================================================================================================================
    render() {


        let { persons, view, team1, team2, team1Score, team2Score, TeamDetails, pointTable } = this.state;
        //==================================================================================================================
        return (
            //========================================Nav bar===============================================
            <div className="container">

                <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                    <a className="navbar-brand">
                        Football Tournament
                    </a>
                    <div className="" id="navbarSupportSystem">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a className="nav-link">
                                    Number of Matches<button className="bn btn-primary rounded ">{TeamDetails.length}</button>

                                </a>
                            </li>
                        </ul>

                    </div>
                </nav>
                <br />


                {view == 4 ? "" : <div>  <button className="bg-primary  text-white p-2 col-1 m-2 border rounded-3" onClick={() => this.showList()} >All Matches</button>
                    <button className="bg-primary  text-white p-2 col-1 m-2 border rounded-3" onClick={() => this.showPT()}>Points Table</button>
                    <button className="bg-primary  text-white p-2 col-1 m-2 border rounded-3" onClick={() => this.showForm()}>New Match</button><br />
                </div>}

                {this.state.view == 2 ?
                    <div>
                        {TeamDetails == "" ? <b>"there are NO Matches"</b> :
                            <div>
                                <h3 className="text-center">Result of the matches so far</h3>
                                <div className="row border">
                                    <div className="col-3  bg-black text-light"><b>Team 1</b></div>
                                    <div className="col-3  bg-black text-light"><b>Team 2</b></div>
                                    <div className="col-3  bg-black text-light"><b>score</b></div>

                                    <div className="col-3 bg-black text-light"><b>Result</b></div>
                                </div>

                                {TeamDetails.map((s1) => (
                                    <div className="row border border-dark">
                                        <div className="col-3 ">{s1.team1}</div>
                                        <div className="col-3 ">{s1.team2}</div>
                                        <div className="col-3 ">{s1.team1Score}-{s1.team2Score}</div>
                                        <div className="col-3 ">{s1.result}</div>



                                    </div>
                                ))}


                            </div>}</div> : null}


                {this.state.view == 1 ?



                    <div className="container">
                        <div className="row text-center">
                            <div className="text-center">
                                {team1 == "" ? <h3><b>Choose Team 1</b></h3> : <h3><b>Team 1 : {team1}</b></h3>}

                            </div>

                            {persons.map((st) => (

                                <button className=" text-center row border col-2 bg-warning m-1 " onClick={() => this.setTeam1(st)}>{st} </button>

                            ))}
                        </div><br /><br />
                        <div className=" row text-center">
                            {team2 == "" ? <h3><b>Choose Team 1</b></h3> : <h3><b>Team 2 : {team2}</b></h3>}



                            {persons.map((st) => (

                                <button className="text-center row border col-2 bg-warning m-1 " onClick={() => this.setTeam2(st)}>{st}</button>

                            ))}
                        </div>  <br /><br />
                        <div className="text-center">
                            <button className="bg-dark text-light rounded col-1 p-2" onClick={() => { this.buzzerClick() }}>Start Match</button>
                        </div> </div>
                    : null}

                {this.state.view == 4 ?
                    <div className="container">
                        <div className="row">
                            <div className="text-center">
                                <h3><b>Welcome to an exciting Match</b></h3>
                            </div >
                            <div className="text-center row">
                                <div className="col-4 row ">
                                    <h4><b>{team1}</b></h4>
                                    <br />
                                    <div className="text-center">
                                        <button className="bg-warning p-1 m-2 col-2 rounded " onClick={() => { this.goalTeam1() }}><h5>Goal</h5></button>
                                    </div>
                                </div>
                                <div className="col-4 row">
                                    <h2><b>{team1Score} - {team2Score}</b></h2>


                                </div>
                                <div className="col-4 row p-2">
                                    <h4><b>{team2}</b></h4>
                                    <br />
                                    <div className="text-center">
                                        <button className="bg-warning p-1 m-2 col-2 rounded" onClick={() => { this.goalTeam2() }}><h5>Goal</h5></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="text-center"> <button className="bg-warning p-1 m-2 col-2 rounded" onClick={() => { this.gameOver() }} ><h5>Match Over</h5></button></div>
                    </div>

                    : null}
                {this.state.view == 5 ? <div>
                    <h3 className="text-center">Point table</h3>
                    <div className="row border">
                        <div className="col-3  bg-black text-light" onClick={() => this.sortPTBy("teamName")}><b>Team</b></div>
                        <div className="col-1  bg-black text-light" onClick={() => this.sortPTBy("played")}><b>Played</b></div>
                        <div className="col-1  bg-black text-light" onClick={() => this.sortPTBy("won")}><b>won</b></div>

                        <div className="col-1 bg-black text-light" onClick={() => this.sortPTBy("loss")}><b>Lost</b></div>
                        <div className="col-1 bg-black text-light" onClick={() => this.sortPTBy("draw")}><b>Drawn</b></div>
                        <div className="col-2 bg-black text-light" onClick={() => this.sortPTBy("goalFor")}><b>Goals For</b></div>
                        <div className="col-2 bg-black text-light" onClick={() => this.sortPTBy("goalAgainst")}><b>Goals Against</b></div>
                        <div className="col-1 bg-black text-light" onClick={() => this.sortPTBy("points")}><b>Points</b></div>
                    </div>

                    {pointTable.map((s1) => (
                        <div className="row border border-dark">
                            <div className="col-3 ">{s1.teamName}</div>
                            <div className="col-1 ">{s1.played}</div>
                            <div className="col-1 ">{s1.won}</div>
                            <div className="col-1 ">{s1.loss}</div>
                            <div className="col-1 ">{s1.draw}</div>
                            <div className="col-2 ">{s1.goalFor}</div>
                            <div className="col-2 ">{s1.goalAgainst}</div>
                            <div className="col-1 ">{s1.points}</div>





                        </div>
                    ))}


                </div> : null}


            </div>


        )

    }
}

export default MainComponent